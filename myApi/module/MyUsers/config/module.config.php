<?php
return [
    'router' => [
        'routes' => [
            'my-users.rest.users' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/users[/:users_id]',
                    'defaults' => [
                        'controller' => 'MyUsers\\V1\\Rest\\Users\\Controller',
                    ],
                ],
            ],
            'my-users.rest.images' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/images[/:images_id]',
                    'defaults' => [
                        'controller' => 'MyUsers\\V1\\Rest\\Images\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'my-users.rest.users',
            2 => 'my-users.rest.images',
        ],
    ],
    'zf-rest' => [
        'MyUsers\\V1\\Rest\\Users\\Controller' => [
            'listener' => 'MyUsers\\V1\\Rest\\Users\\UsersResource',
            'route_name' => 'my-users.rest.users',
            'route_identifier_name' => 'users_id',
            'collection_name' => 'users',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \MyUsers\V1\Rest\Users\UsersEntity::class,
            'collection_class' => \MyUsers\V1\Rest\Users\UsersCollection::class,
            'service_name' => 'users',
        ],
        'MyUsers\\V1\\Rest\\Images\\Controller' => [
            'listener' => \MyUsers\V1\Rest\Images\ImagesResource::class,
            'route_name' => 'my-users.rest.images',
            'route_identifier_name' => 'images_id',
            'collection_name' => 'images',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \MyUsers\V1\Rest\Images\ImagesEntity::class,
            'collection_class' => \MyUsers\V1\Rest\Images\ImagesCollection::class,
            'service_name' => 'images',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'MyUsers\\V1\\Rest\\Users\\Controller' => 'HalJson',
            'MyUsers\\V1\\Rest\\Images\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'MyUsers\\V1\\Rest\\Users\\Controller' => [
                0 => 'application/vnd.my-users.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'MyUsers\\V1\\Rest\\Images\\Controller' => [
                0 => 'application/vnd.my-users.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'MyUsers\\V1\\Rest\\Users\\Controller' => [
                0 => 'application/vnd.my-users.v1+json',
                1 => 'application/json',
                2 => 'multipart/mixed',
                3 => 'multipart/form-data',
            ],
            'MyUsers\\V1\\Rest\\Images\\Controller' => [
                0 => 'application/vnd.my-users.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
                3 => 'multipart/mixed',
                4 => 'image/png',
                5 => 'application/octet-stream',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \MyUsers\V1\Rest\Users\UsersEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'my-users.rest.users',
                'route_identifier_name' => 'users_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \MyUsers\V1\Rest\Users\UsersCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'my-users.rest.users',
                'route_identifier_name' => 'users_id',
                'is_collection' => true,
            ],
            \MyUsers\V1\Rest\Images\ImagesEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'my-users.rest.images',
                'route_identifier_name' => 'images_id',
                'hydrator' => \Zend\Hydrator\ObjectProperty::class,
            ],
            \MyUsers\V1\Rest\Images\ImagesCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'my-users.rest.images',
                'route_identifier_name' => 'images_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-apigility' => [
        'db-connected' => [
            'MyUsers\\V1\\Rest\\Users\\UsersResource' => [
                'adapter_name' => 'MysqlAdapter',
                'table_name' => 'users',
                'hydrator_name' => \Zend\Hydrator\ArraySerializable::class,
                'controller_service_name' => 'MyUsers\\V1\\Rest\\Users\\Controller',
                'entity_identifier_name' => 'id',
                'table_service' => 'MyUsers\\V1\\Rest\\Users\\UsersResource\\Table',
            ],
        ],
    ],
    'zf-content-validation' => [
        'MyUsers\\V1\\Rest\\Users\\Controller' => [
            'input_filter' => 'MyUsers\\V1\\Rest\\Users\\Validator',
        ],
        'MyUsers\\V1\\Rest\\Images\\Controller' => [
            'input_filter' => 'MyUsers\\V1\\Rest\\Images\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'MyUsers\\V1\\Rest\\Users\\Validator' => [
            0 => [
                'name' => 'name',
                'required' => true,
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '255',
                        ],
                    ],
                ],
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'max' => '255',
                            'min' => '1',
                        ],
                    ],
                ],
                'filters' => [],
                'name' => 'img',
            ],
            2 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'max' => '255',
                            'min' => '1',
                        ],
                    ],
                ],
                'filters' => [],
                'name' => 'email',
            ],
        ],
        'MyUsers\\V1\\Rest\\Heros\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\File\IsImage::class,
                        'options' => [
                            'mimeType' => 'png',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\File\RenameUpload::class,
                        'options' => [
                            'target' => 'C:\\xampp\\htdocs\\myApi\\data\\img',
                            'use_upload_name' => true,
                        ],
                    ],
                ],
                'name' => 'file',
                'type' => \Zend\InputFilter\FileInput::class,
            ],
        ],
        'MyUsers\\V1\\Rest\\Images\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\File\IsImage::class,
                        'options' => [
                            'message' => 'Você deve enviar um arquivo do tipo .png ou .jpg',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\File\RenameUpload::class,
                        'options' => [
                            'randomize' => true,
                            'target' => 'C:\\xampp\\htdocs\\ProjectApi\\projetoangularapigility\\cdk-admin\\src\\assets\\images',
                            'use_upload_name' => true,
                        ],
                    ],
                ],
                'name' => 'file',
                'type' => \Zend\InputFilter\FileInput::class,
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            \MyUsers\V1\Rest\Images\ImagesResource::class => \MyUsers\V1\Rest\Images\ImagesResourceFactory::class,
        ],
    ],
];
