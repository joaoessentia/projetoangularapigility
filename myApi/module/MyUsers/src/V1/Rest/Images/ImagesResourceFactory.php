<?php
namespace MyUsers\V1\Rest\Images;

class ImagesResourceFactory
{
    public function __invoke($services)
    {
        return new ImagesResource();
    }
}
