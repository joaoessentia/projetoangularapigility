import { id } from '@swimlane/ngx-charts/release/utils';
import { query } from '@angular/animations';
import { EditorComponent } from './../../editor/editor.component';
import { Component, OnInit, ElementRef } from '@angular/core';
import { DataService } from './../../data.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  checked = false;
  indeterminate = false;

  users: Object;
  userImage: any;

  constructor(
    private data: DataService
    ) { }

  ngOnInit() {
    this.userImage =false;
    this.data.getUsers().subscribe(
      data => {
        this.users = data['_embedded']['users'];
        this.userImage = true;
      }
    );
  }

  deleteUser(id: number, img: any) {
    this.data.deleteUser(id).subscribe(response => {
      console.log(response);
      this.data.deleteImg(img);
    });
  }
}
