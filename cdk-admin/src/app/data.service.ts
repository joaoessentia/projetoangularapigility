import { User } from './forms/template-driven-forms/user';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { id } from '@swimlane/ngx-charts/release/utils';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  users = 'http://localhost:9999/users';
  images = 'http://localhost:9999/images';

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get(this.users);
  }

  getUser(userId: any) {
    return this.http.get(this.users + '/' + userId);
  }

  newUser(user: any) {
    return this.http.post(this.users, user);
  }

  deleteUser(id: number) {
    return this.http.delete(this.users + '/' + id);
  }

  updateUser(userID: number,  user: any) {
    return this.http.put(this.users + '/' + userID , user);
  }
  uploadImgUser(img: any) {
    return this.http.post(this.images, img);
  }

  deleteImg(img: any) {
    console.log('entrou no service - deleteig');
    return this.http.delete(this.images + '/' + img)
      .subscribe(response => {
        console.log(response);
        window.location.reload();
      });
  }
}
