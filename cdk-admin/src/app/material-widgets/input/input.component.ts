import { catchError } from 'rxjs/operators';
import { DashboardAccountsComponent } from './../../dashboard-accounts/dashboard-accounts.component';
import { Component, OnInit, ElementRef  } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { input_HELPERS, Messages, Links } from './helpers.data';
import { DataService } from './../../data.service';
import { FormGroup } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

@Component({
  selector: 'cdk-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})

export class InputComponent implements OnInit {
  InputHelpers: any = input_HELPERS;
  links = Links;
  selectedValue;
  showMultiListCode: boolean = false;
  messages = Messages;
  value = 'Clear me';
  emailFormControl = new FormControl('', [
  Validators.required,
  Validators.email,
  ]);
  emailFormControls = new FormControl('', [
  Validators.required,
  Validators.pattern(EMAIL_REGEX)]);
  matcher = new MyErrorStateMatcher();

  @ViewChild('myInput')
  myInput: ElementRef;
  user: any;
  userId: any;
  formData = new FormData;
  editMode: boolean;
  existingFile: boolean;
  snackMessage: string;

  constructor(
    public snackBar: MatSnackBar,
    private data: DataService,
    private router: ActivatedRoute
    ) { }

  ngOnInit() {
    this.existingFile = false;
    this.user = {};
    this.checkEditMode();
  }

  checkEditMode() {
    const idUrl = this.router.snapshot.params['id'];
    if (idUrl !== 'add') {
      this.editMode = true;
      this.snackMessage = 'User edited successfully';
      this.getUser(idUrl);
    } else {
      this.editMode = false;
      this.snackMessage = 'User created successfully';
    }
  }

  getUser(userId: number) {
    this.data.getUser(userId).subscribe(
      data => {
        this.user = {
          name: data['name'],
          email: data['email'],
          img: data['img']
        };
        this.userId = data['id'];
      }
    );
  }

  inputFileChange(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      this.formData.append('file', file);
      this.existingFile = true;
    }
  }

  createOrUpdateUser(frm: FormGroup) {
    if (this.editMode === false) {
      this.sendImageAndUserDatas(frm);
    } else {
      this.updateUser(frm);
    }
  }

  updateUser(frm: FormGroup) {
    if (this.existingFile === true) {
      this.data.deleteImg(this.user['img']);
      this.sendImageAndUserDatas(frm);
    } else {
      this.sendUserDatas(frm);
    }
  }

  sendImageAndUserDatas(frm: FormGroup) {
    this.data.uploadImgUser(this.formData)
      .subscribe(
        response => {
          this.user['img'] = response['file'].tmp_name.split('assets\\images\\')[1];
          this.sendUserDatas(frm);
        }
      );
  }

  sendUserDatas(frm: FormGroup) {
    if (this.editMode === false) {
      this.data.newUser(this.user).subscribe(response => {
        console.log(response);
      });
    } else {
      this.data.updateUser(this.userId, this.user).subscribe(response => {
        console.log(response);
      });
    }
    this.resetMyForms(frm);
  }

  resetMyForms(frm: FormGroup) {
    frm.reset();
    this.emailFormControls.reset();
    this.myInput.nativeElement.value = null;
    this.snackOpen();
  }

  snackOpen() {
    const actionButtonLabel = 'OK';
    const action = true;
    const setAutoHide = true;
    const autoHide = 2000;
    const horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    const verticalPosition: MatSnackBarVerticalPosition = 'bottom';
    const addExtraClass = false;
    const config = new MatSnackBarConfig();
    config.verticalPosition = verticalPosition;
    config.horizontalPosition = horizontalPosition;
    config.duration = setAutoHide ? autoHide : 0;
    config.panelClass = ['red-snackbar'];
    this.snackBar.open(this.snackMessage, actionButtonLabel , config);
  }

}
